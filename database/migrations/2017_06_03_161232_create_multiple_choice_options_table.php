<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultipleChoiceOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multiple_choice_options', function (Blueprint $table) {
            $table->increments('id');
            $table->text('answer');
            $table->integer('multiple_choice_id')->unsigned();
            $table->foreign('multiple_choice_id')->references('id')->on('multiple_choice_questions')->onDelete('cascade')->onUpdate('cascade');
            $table->enum('right_answer',['yes','no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multiple_choice_options');
    }
}
