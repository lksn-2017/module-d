<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Classroom;
use App\Student;
use App\Teacher;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User::truncate();
        // Classroom::truncate();

        $user = new User;
        $user->username = 'revando';
        $user->password = bcrypt('revando');
        $user->role = 'student';
        $user->save();

        $user2 = new User;
        $user2->username = 'ichsan';
        $user2->password = bcrypt('ichsan');
        $user2->role = 'student';
        $user2->save();

        $user3 = new User;
        $user3->username = 'aroh';
        $user3->password = bcrypt('aroh');
        $user3->role = 'teacher';
        $user3->save();

        $class = new Classroom;
        $class->name = 'XII RPL';
        $class->save();

        $class2 = new Classroom;
        $class2->name = 'XII PM';
        $class2->save();

        $student = new Student;
        $student->name = 'Revando';
        $student->classroom_id = $class->id;
        $student->user_id = $user->id;
        $student->save();

        $student2 = new Student;
        $student2->name = 'Ichsan';
        $student2->classroom_id = $class->id;
        // $student2->classroom_id = $class2->id;
        $student2->user_id = $user2->id;
        $student2->save();

        $teacher = new Teacher;
        $teacher->name = 'Aroh';
        $teacher->user_id = $user3->id;
        $teacher->save();
    }
}
