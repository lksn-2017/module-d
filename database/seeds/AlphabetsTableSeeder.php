<?php

use Illuminate\Database\Seeder;
use App\Alphabet;

class AlphabetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Alphabet::create(['alphabet'=>'a']);
        Alphabet::create(['alphabet'=>'b']);
        Alphabet::create(['alphabet'=>'c']);
        Alphabet::create(['alphabet'=>'d']);
        Alphabet::create(['alphabet'=>'e']);
    }
}
