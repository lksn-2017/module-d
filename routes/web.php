<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('login', 'Auth\LoginController@getLogin')->name('getLogin');
Route::post('login', 'Auth\LoginController@postLogin')->name('postLogin'); //Validation clear
Route::get('logout', 'Auth\LoginController@logout')->name('logout'); //Validation clear

Route::group(['prefix'=>'teachers','middleware'=>'role:teacher'],function(){
  Route::get('/', 'TeacherController@index')->name('teacherHome');
  Route::post('/', 'TeacherController@changePassword')->name('teacherChangePassword'); //Validation clear
  Route::group(['prefix'=>'exams'],function(){
    Route::get('/', 'TeacherController@examIndex')->name('teacherExamHome');
    Route::post('/', 'TeacherController@postExam')->name('teacherPostExam'); //Validation clear
    Route::get('{id}/destroy', 'TeacherController@destroyExam')->name('teacherDestroyExam');
    Route::get('{id}/manage', 'TeacherController@manageExam')->name('teacherManageExam');
    Route::post('{id}/manage/mc', 'TeacherController@saveMultipleChoice')->name('teacherSaveMultipleChoice'); //Validation clear
    Route::post('{id}/manage/essay', 'TeacherController@saveEssay')->name('teacherSaveEssay'); //Validation clear
    Route::get('{id}/questions/mc/{question_id}/destroy', 'TeacherController@deleteMultipleChoiceQuestion')->name('teacherDeleteMultipleChoiceQuestion');
    Route::get('{id}/questions/eq/{question_id}/destroy', 'TeacherController@deleteEssayQuestion')->name('teacherDeleteEssayQuestion');

    Route::group(['prefix'=>'assess'], function(){
      Route::get('/', 'TeacherController@assessList')->name('teacherAssessList');
      Route::get('{id}', 'TeacherController@assessExam')->name('teacherAssessExam');
      Route::get('{id}/student/{student_id}', 'TeacherController@assessStudentExam')->name('teacherAssessStudentExam');
      Route::post('{id}/student/{student_id}', 'TeacherController@assessStudentExamProcess')->name('teacherAssessStudentExamProcess');
    });

    Route::group(['prefix'=>'statistics'], function(){
      Route::get('/', 'TeacherController@statisticList')->name('teacherStatisticList');
      Route::get('{classroom_id}/ranks', 'TeacherController@rankList')->name('teacherStatisticRankList');
    });
  });
});

Route::group(['prefix'=>'students','middleware'=>'role:student'],function(){
  Route::get('/', 'StudentController@index')->name('studentHome');
  Route::post('/', 'StudentController@changePassword')->name('studentChangePassword'); //Validation clear
  Route::group(['prefix'=>'exams'],function(){
    Route::get('/', 'StudentController@examList')->name('studentExamList');
    Route::get('scores', 'StudentController@examScore')->name('studentExamScore');
    Route::get('{student_exam_id}/take', 'StudentController@takeExam')->name('studentTakeExam');
    Route::post('{student_exam_id}/take', 'StudentController@submitAnswer')->name('studentSubmitAnswer');
  });
});
