<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exam;
use App\StudentExam;
use App\Classroom;
use App\MultipleChoiceQuestion as MC;
use App\MultipleChoiceOption;
use App\EssayQuestion as EQ;
use App\StudentMultipleChoiceAnswer as SMCA;
use App\StudentEssayAnswer as SEA;
use App\HistoryStudentMultipleChoiceAnswer as HSMCA;
use App\HistoryStudentEssayAnswer as HSEA;
use App\Http\Controllers\HelperController as HelpMe;
use App\Student;
use DB;
use Validator;
use App\User;
use Auth;

class StudentController extends Controller
{
    public function __construct()
    {
      return $this->middleware('auth');
    }

    public function index()
    {
      return view('exam.students.home');
    }

    public function changePassword(Request $r)
    {
      $old_password = $r->input('old_password');
      $new_password = $r->input('new_password');
      $conf_password = $r->input('conf_password');

      $validator = Validator::make($r->all(),[
        'old_password'=>'required',
        'new_password'=>'required',
        'conf_password'=>'required|same:new_password',
      ]);

      if ($validator->fails()) {
        return redirect()->route('studentHome')->withErrors($validator);
      }

      $user = User::find(Auth::user()->id);
      $user->password = bcrypt($new_password);
      $user->save();

      return redirect()->route('studentHome');
    }

    public function examList()
    {
      $help_me = new HelpMe;
      $student_exams = StudentExam::where('student_id',$help_me->user())->where(function($query){
        $query->where('status','not started yet')
              ->orWhere('status','was working');
      })
      ->orderBy('id','desc')
      ->get();
      return view('exam.students.exams.index',['student_exams'=>$student_exams]);
    }

    public function takeExam($student_exam_id)
    {
      $student_exam = StudentExam::find($student_exam_id);

      if ($student_exam->status == 'not started yet') {
        $student_exam->status = 'was working';
        $student_exam->save();
      }

      $exam = Exam::find($student_exam->exam_id);
      $classroom = Classroom::find($exam->classroom_id);
      $mc = MC::where('exam_id',$exam->id)->inRandomOrder()->get();
      $eq = EQ::where('exam_id',$exam->id)->inRandomOrder()->get();
      return view('exam.students.exams.take',['student_exam'=>$student_exam,'exam'=>$exam,'classroom'=>$classroom,'mc'=>$mc,'eq'=>$eq]);
    }

    public function submitAnswer($student_exam_id, Request $r)
    {
      $student_exam = StudentExam::find($student_exam_id);

      if ($student_exam->status == 'was working') {
        $student_exam->status = 'already completed';
        $student_exam->save();
      }

      $help_me = new HelpMe;

      $exam = Exam::find($student_exam->exam_id);
      $classroom = Classroom::find($exam->classroom_id);
      $mc = MC::where('exam_id',$exam->id)->get();
      $eq = EQ::where('exam_id',$exam->id)->get();

      foreach ($mc as $mc_value) {
        $option_id = $r->input('choice-'.$mc_value->id);
        $answer = MultipleChoiceOption::find($option_id);

          $smca = new SMCA;
          $smca->question_id = $mc_value->id;
          $smca->option_id = $option_id;
          $smca->student_id = $help_me->user();
          $smca->save();

          $hsmca = new HSMCA;
          $hsmca->question_id = $mc_value->id;
          $hsmca->option_id = $option_id;
          $hsmca->student_id = $help_me->user();
          $hsmca->save();

          if ($answer->right_answer == 'yes') {
            $student_exam->score = $student_exam->score + $mc_value->weight;
            $student_exam->save();
          }
      }

      foreach ($eq as $eq_value) {
        $answer_text = $r->input('essay-'.$eq_value->id);

        $sea = new SEA;
        $sea->answer = $answer_text;
        $sea->essay_question_id = $eq_value->id;
        $sea->student_id = $help_me->user();
        $sea->save();

        $hsea = new HSEA;
        $hsea->answer = $answer_text;
        $hsea->essay_question_id = $eq_value->id;
        $hsea->student_id = $help_me->user();
        $hsea->save();
      }

      return redirect()->route('studentExamList');
    }

    public function examScore()
    {
      $help_me = new HelpMe;
      $student = Student::find($help_me->user());

      $student_exams = StudentExam::where('student_id', $help_me->user())->where('status', 'already completed')->orderBy('id','desc')->get();
      $avg_score = StudentExam::where('student_id', $help_me->user())->where('status', 'already completed')->avg('score');
      $student_in_class = Student::where('classroom_id', $student->classroom_id)->count();
      $student_in_school = Student::count();

      $no_rank_in_class = 0;
      $score_in_class = StudentExam::select(DB::raw('avg(score) as avg_score'),'student_id')->where('classroom_id', $student->classroom_id)->where('status','already completed')->groupBy('student_id')->orderBy('avg_score','desc')->get();
      $find_rank_in_class = 0;

      foreach ($score_in_class as $value) {
        $no_rank_in_class++;
        if ($value->student_id == $student->id) {
          $find_rank_in_class = $no_rank_in_class;
          break;
        }
      }

      $no_rank_in_school = 0;
      $score_in_school = StudentExam::select(DB::raw('avg(score) as avg_score'),'student_id')->where('status','already completed')->groupBy('student_id')->orderBy('avg_score','desc')->get();
      $find_rank_in_school = 0;

      foreach ($score_in_school as $value) {
        $no_rank_in_school++;
        if ($value->student_id == $student->id) {
          $find_rank_in_school = $no_rank_in_school;
          break;
        }
      }

      return view('exam.students.exams.score', ['student_exams'=>$student_exams, 'avg_score'=>$avg_score, 'student_in_class'=>$student_in_class, 'student_in_school'=>$student_in_school, 'rank_in_class'=>$find_rank_in_class, 'rank_in_school'=>$find_rank_in_school]);
    }
}
