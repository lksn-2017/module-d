<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exam;
use App\Classroom;
use App\Alphabet;
use App\Student;
use App\StudentExam;
use Carbon;
use App\Http\Controllers\HelperController as HelpMe;
use App\MultipleChoiceQuestion as MC;
use App\MultipleChoiceOption as MCO;
use App\EssayQuestion as EQ;
use App\EssayKeyword as EK;
use App\StudentMultipleChoiceAnswer as SMCA;
use DB;
use Validator;
use App\User;
use Auth;

class TeacherController extends Controller
{
    public function __construct()
    {
      return $this->middleware('auth');
    }

    public function index()
    {
      return view('exam.teachers.home');
    }

    public function changePassword(Request $r)
    {
      $old_password = $r->input('old_password');
      $new_password = $r->input('new_password');
      $conf_password = $r->input('conf_password');

      $validator = Validator::make($r->all(),[
        'old_password'=>'required',
        'new_password'=>'required',
        'conf_password'=>'required|same:new_password',
      ]);

      if ($validator->fails()) {
        return redirect()->route('teacherHome')->withErrors($validator);
      }

      $user = User::find(Auth::user()->id);
      $user->password = bcrypt($new_password);
      $user->save();

      return redirect()->route('teacherHome');
    }

    public function examIndex()
    {
      $help_me = new HelpMe;

      $exams = Exam::where('created_by', $help_me->user())->orderBy('id','desc')->get();
      $classrooms = Classroom::all();
      return view('exam.teachers.exams.index',['exams'=>$exams,'classrooms'=>$classrooms]);
    }

    public function postExam(Request $r)
    {
      $title = $r->input('title');
      $classroom_id = $r->input('classroom_id');
      $start_date = $r->input('start_date');
      $start_time = $r->input('start_time');
      $end_date = $r->input('end_date');
      $end_time = $r->input('end_time');

      $validator = Validator::make($r->all(),[
        'title'=>'required',
        'classroom_id'=>'required',
        'start_date'=>'required',
        'start_time'=>'required',
        'end_date'=>'required',
        'end_time'=>'required',
      ]);

      if ($validator->fails()) {
        return redirect()->route('teacherExamHome')->withErrors($validator);
      }

      $exam = new Exam;
      $exam->title = $title;
      $exam->start = $start_date.' '.$start_time.':00';
      $exam->end = $end_date.' '.$end_time.':00';

      $help_me = new HelpMe;

      $exam->created_by = $help_me->user();
      $exam->classroom_id = $classroom_id;
      $exam->save();

      $students = Student::where('classroom_id',$classroom_id)->get();

      foreach ($students as $student) {
        $student_exam = new StudentExam;
        $student_exam->classroom_id = $classroom_id;
        $student_exam->student_id = $student->id;
        $student_exam->exam_id = $exam->id;
        $student_exam->status = 'not started yet';
        $student_exam->score = 0;
        $student_exam->is_corrected = 'no';
        $student_exam->save();
      }

      return redirect()->route('teacherExamHome');
    }

    public function destroyExam($id)
    {
      $exam = Exam::find($id);
      $exam->delete();

      return redirect()->route('teacherExamHome');
    }

    public function manageExam($id)
    {
      $exam = Exam::find($id);
      $classroom = Classroom::find($exam->classroom_id);
      $alphabets = Alphabet::all();
      $mc = MC::where('exam_id',$id)->get();
      $essay_questions = EQ::where('exam_id',$id)->get();

      return view('exam.teachers.exams.manage',['exam'=>$exam,'classroom'=>$classroom,'alphabets'=>$alphabets,'mc'=>$mc,'essay_questions'=>$essay_questions]);
    }

    public function saveMultipleChoice($id, Request $r)
    {
      $question = $r->input('question');
      $weight = $r->input('weight');
      $right_answer = $r->input('right');

      $alphabets = Alphabet::all();

      $validator = Validator::make($r->all(),[
        'question'=>'required',
        'weight'=>'required',
        'right'=>'required',
      ]);
      
      if ($validator->fails()) {
        return redirect()->route('teacherManageExam',$id)->withErrors($validator, 'mc');
      }

      $mc = new MC;
      $mc->question = $question;
      $mc->weight = $weight;
      $mc->exam_id = $id;
      $mc->save();

      foreach ($alphabets as $alphabet) {
        $answer = new MCO;
        $answer->answer = $r->input('option_'.$alphabet->id);
        $answer->multiple_choice_id = $mc->id;

        if ($right_answer == $alphabet->id) {
          $answer->right_answer = 'yes';
        }
        else {
          $answer->right_answer = 'no';
        }

        $answer->save();
      }

      return redirect()->route('teacherManageExam',$id);
    }

    public function saveEssay($id, Request $r)
    {
      $question = $r->input('question');
      $weight = $r->input('weight');
      $keywords = $r->input('keywords');

      $validator = Validator::make($r->all(),[
        'question'=>'required',
        'keywords'=>'required',
        'weight'=>'required',
      ]);

      if ($validator->fails()) {
        return redirect()->route('teacherManageExam',$id)->withErrors($validator, 'essay');
      }

      $count_keyword = count(explode('|',$keywords));

      $eq = new EQ;
      $eq->question = $question;
      $eq->weight = $weight;
      $eq->exam_id = $id;
      $eq->save();

      for ($i = 0; $i < $count_keyword; $i++) {
        $ek = new EK;
        $ek->keyword = explode('|',$keywords)[$i];
        $ek->essay_id = $eq->id;
        $ek->save();
      }

      return redirect()->route('teacherManageExam',$id);
    }

    public function deleteMultipleChoiceQuestion($id,$question_id)
    {
      $mc = MC::find($question_id);
      $mc->delete();

      return redirect()->route('teacherManageExam',$id);
    }

    public function deleteEssayQuestion($id,$question_id)
    {
      $eq = EQ::find($question_id);
      $eq->delete();

      return redirect()->route('teacherManageExam',$id);
    }

    public function assessList()
    {
      $help_me = new HelpMe;

      $exams = Exam::where('created_by',$help_me->user())->orderBy('id','desc')->get();
      return view('exam.teachers.exams.assess',['exams'=>$exams]);
    }

    public function assessExam($id)
    {
      $exam = Exam::find($id);
      $classroom = Classroom::find($exam->classroom_id);
      $student_exams = StudentExam::where('exam_id',$exam->id)->where('status','already completed')->get();
      return view('exam.teachers.exams.assess_exam',['exam'=>$exam, 'student_exams'=>$student_exams, 'classroom'=>$classroom]);
    }

    public function assessStudentExam($id, $student_id)
    {
      $exam = Exam::find($id);
      $student = Student::find($student_id);
      $classroom = Classroom::find($exam->classroom_id);
      $mc = MC::where('exam_id', $id)->get();
      $eq = EQ::where('exam_id', $id)->get();
      $sum_right_mc = 0;
      return view('exam.teachers.exams.assess_student_exam',['exam'=>$exam, 'student'=>$student, 'classroom'=>$classroom, 'mc'=>$mc, 'eq'=>$eq, 'sum_right_mc'=>$sum_right_mc]);
    }

    public function assessStudentExamProcess($id, $student_id, Request $r)
    {
      $exam = Exam::find($id);
      $mc = MC::where('exam_id', $id)->get();
      $eq = EQ::where('exam_id', $id)->get();
      $mc_value = 0;
      $essay_value = 0;

      foreach ($mc as $mcq) {
        $answers = MCO::where('multiple_choice_id', $mcq->id)->where('right_answer', 'yes')->get();

        foreach ($answers as $answer) {
          $student_answer_mc = SMCA::where('question_id',$mcq->id)->where('option_id',$answer->id)->where('student_id', $student_id)->first();

          if (count($student_answer_mc) > 0) {
            $mc_value = $mc_value + $mcq->weight;
          }
        }
      }

      foreach ($eq as $eqs) {
        $essay_score = $r->input('score-essay-'.$eqs->id);
        $essay_value = $essay_value + $essay_score;
      }

      $find_student_exam = StudentExam::where('exam_id', $id)->where('student_id', $student_id)->first();

      $student_exam = StudentExam::find($find_student_exam->id);
      $student_exam->score = $mc_value + $essay_value;
      $student_exam->save();

      return redirect()->route('teacherAssessExam',$id);
    }

    public function statisticList()
    {
      $help_me = new HelpMe;

      $exams = Exam::where('created_by', $help_me->user())->orderBy('id','desc')->get();
      $classrooms = Classroom::all();

      return view('exam.teachers.exams.statistic',['exams'=>$exams, 'classrooms'=>$classrooms]);
    }

    public function rankList($classroom_id)
    {
      $classroom = Classroom::find($classroom_id);
      $student_exams = StudentExam::select(DB::raw('avg(score) as avg_score'),'student_id')->where('classroom_id', $classroom_id)->where('status', 'already completed')->orderBy('avg_score','desc')->groupBy('student_id')->get();
      $no = 1;
      return view('exam.teachers.exams.rank', ['classroom'=>$classroom, 'student_exams'=>$student_exams, 'no'=>$no]);
    }
}
