<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Student;
use App\Teacher;

class HelperController extends Controller
{
    public function user()
    {
      $user_id = Auth::user()->id;

      if (Auth::user()->role == 'teacher') {
        $teacher = Teacher::where('user_id',$user_id)->first();
        return $teacher->id;
      }

      else if (Auth::user()->role == 'student') {
        $student = Student::where('user_id',$user_id)->first();
        return $student->id;
      }
    }
}
