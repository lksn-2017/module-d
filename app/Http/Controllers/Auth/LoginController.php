<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use App\LoginLog;
use Auth;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getLogin()
    {
      return view('exam.auth.login');
    }

    public function postLogin(Request $r)
    {
      $validator = Validator::make($r->all(),[
        'username'=>'required',
        'password'=>'required',
      ]);

      if ($validator->fails()) {
        return redirect()->route('getLogin')->withErrors($validator);
      }

      $username = $r->input('username');
      $password = $r->input('password');

      if (Auth::attempt(['username'=>$username,'password'=>$password],true)) {
        $log = new LoginLog;
        $log->login_time = date('Y-m-d H:i:s');
        $log->ip_address = $r->ip();
        $log->user_id = Auth::user()->id;
        $log->save();

        if (Auth::user()->role == 'student') {
          return redirect()->route('studentHome');
          // return 'student';
        }

        else if (Auth::user()->role == 'teacher') {
          return redirect()->route('teacherHome');
          // return 'teacher';
        }
      }

      $message = 'Invalid username or password';
      return redirect()->route('getLogin')->with('message',$message);
    }

    public function logout()
    {
      if (Auth::check()) {
        Auth::logout();
        return redirect()->route('getLogin');
      }

      $message = 'You must login first.';
      return redirect()->route('getLogin')->with('message',$message);
    }
}
