<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    public function index()
    {
      if (Auth::check()) {
        if (Auth::user()->role == 'student') {
          return redirect()->route('studentHome');
          // return 'student';
        }

        else if (Auth::user()->role == 'teacher') {
          return redirect()->route('teacherHome');
          // return 'teacher';
        }
      }

      return redirect()->route('getLogin');
    }
}
