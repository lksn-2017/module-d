@if($errors)
  @foreach($errors->all() as $error)
  <div style="color:red">
    {{ $error }}
  </div>
  @endforeach
@endif
@if(session('message'))
<div>
  {{ session('message') }}
</div>
@endif
