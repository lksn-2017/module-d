
<!DOCTYPE html>
<html>
<head>
	<title>Exam</title>
	@include('exam.layouts.asset')
</head>
<body>
	<nav>
		<span>
			@if(Auth::user()->role == 'teacher')
			<a href="{{ route('teacherHome') }}">Home</a>
			<a href="{{ route('teacherExamHome') }}">Create Exam</a>
			<a href="{{ route('teacherAssessList') }}">Assess Exam</a>
			<a href="{{ route('teacherStatisticList') }}">Statistic</a>
			@else
			<a href="{{ route('studentHome') }}">Home</a>
			<a href="{{ route('studentExamList') }}">Answer Exam</a>
			<a href="{{ route('studentExamScore') }}">View Score</a>
			@endif
			<a href="{{ route('logout') }}">Logout</a>
		</span>
	</nav>
	@yield('content')
</body>
</html>
