@extends('exam.layouts.app')

@section('content')

<div class="exam-data">
<div>{{ $exam->title }} kelas {{ $classroom->name }}</div>
<div>{{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->start)->format('j F Y H:i:s') }} - {{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->end)->format('j F Y H:i:s') }}</div>
</div>
<div class="multiple-choice-wrapper">
<h3 style="text-align:center">Multiple Choice</h3>
<div class="form-wrapper">
	<form action="{{ route('teacherSaveMultipleChoice',$exam->id) }}" method="POST">
		{{ csrf_field() }}
		<div>
			<input type="text" name="question" placeholder="Question">
		 </div>
		@foreach($alphabets as $alphabet)
		<div>
			<input type="text" name="option_{{ $alphabet->id }}" placeholder="option{{ strtoupper($alphabet->alphabet) }}">
		</div>
		@endforeach
		<div>
			<input type="number" name="weight" placeholder="Score Weight" max="100" min="0">
		</div>

		<div>
			<select name="right">
				<option selected value="">--Select Correct Choice--</option>
				@foreach($alphabets as $alphabet)
				<option value="{{ $alphabet->id }}">{{ strtoupper($alphabet->alphabet) }}</option>
				@endforeach
			</select>
		</div>
		<div>
			<input type="submit" value="Insert Question" class="button">
		</div>
	</form>
	@if($errors->mc)
	  @foreach($errors->mc->all() as $error)
	  <div style="color:red">
	    {{ $error }}
	  </div>
	  @endforeach
	@endif
</div>
<div>
	<div class="multiple-choice">
		@foreach($mc as $mcq)
		<div class="question">
			{{ $mcq->question }}
			<span class="weight">
				(Weight: {{ $mcq->weight }}%)
			</span>
			<span class="button"><a href="{{ route('teacherDeleteMultipleChoiceQuestion',['id'=>$exam->id,'question_id'=>$mcq->id]) }}">Delete</a></span>
			<div class="answer">
				<table>
					@php
					$no = 0;
					$answers = App\MultipleChoiceOption::where('multiple_choice_id',$mcq->id)->get();
					@endphp
					@foreach($answers as $answer)
					@php
					$no++;
					$alphabet_answer = App\Alphabet::find($no);
					@endphp
					<tr>
						<td @if($answer->right_answer == 'yes') class="correct-answer" @endif>{{ strtoupper($alphabet_answer->alphabet) }}. {{ $answer->answer }}</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
		@endforeach
	</div>
</div>
</div>

<div class="essay-wrapper">
<h3 style="text-align:center">Essay</h3>
<div class="form-wrapper">
	<form action="{{ route('teacherSaveEssay',$exam->id) }}" method="POST">
		{{ csrf_field() }}
		<div>
			<input type="text" name="question" placeholder="Question">
		</div>
		<div>
			<input type="text" name="keywords" placeholder="Keywords">
		</div>
		<div>
			<input type="number" name="weight" placeholder="Score Weight" max="100" min="0">
		</div>
		<div>
			<input type="submit" value="Insert Question" class="button">
		</div>
	</form>
	@if($errors->essay)
	  @foreach($errors->essay->all() as $error)
	  <div style="color:red">
	    {{ $error }}
	  </div>
	  @endforeach
	@endif
</div>
<div>
	<div class="essay">
		@foreach($essay_questions as $essay_question)
		<div class="question">
			{{ $essay_question->question }}
			<span class="weight">
				(Weight: {{ $essay_question->weight }}%)
			</span>
			<span class="keywords">
				Keyword:
				@php
				$essay_keywords = App\EssayKeyword::where('essay_id',$essay_question->id)->get();
				$no = 0;
				@endphp
				@foreach($essay_keywords as $essay_keyword)
				@php
				$no++;
				@endphp
				{{ $essay_keyword->keyword }} @if(count($essay_keywords) > 1 && $no != count($essay_keywords))|@endif
				@endforeach
			</span>
			<span class="button"><a href="{{ route('teacherDeleteEssayQuestion',['id'=>$exam->id,'question_id'=>$essay_question->id]) }}">Delete</a></span>
		</div>
		@endforeach
	</div>
</div>

@endsection
