@extends('exam.layouts.app')

@section('content')

<div class="exam-data">
<div>{{ $exam->title }} kelas {{ $classroom->name }}</div>
<div>{{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->start)->format('j F Y H:i:s') }} - {{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->end)->format('j F Y H:i:s') }}</div>
<div>{{ $student->id }} / {{ $student->name }}</div>
</div>

<div class="multiple-choice-wrapper">
<h3 style="text-align:center">Multiple Choice</h3>
<div>
	<div class="multiple-choice">
		@foreach($mc as $mcq)
		<div class="question">
			{{ $mcq->question }}
			<span class="weight">
				(Weight: {{ $mcq->weight }}%)
			</span>
			<div class="answer">
				@php
				$answers = App\MultipleChoiceOption::where('multiple_choice_id', $mcq->id)->get();
				$no = 0;
				@endphp
				<table>
					@foreach($answers as $answer)
						@php
						$no++;
						$alphabet = App\Alphabet::find($no);
						$student_answer_mc = App\StudentMultipleChoiceAnswer::where('question_id',$mcq->id)->where('option_id',$answer->id)->where('student_id',$student->id)->first();
						@endphp
					<tr>
						<!-- <td class="correct-answer choosen-answer">A. 100</td> -->
						@if($answer->right_answer == 'yes')
							@if(count($student_answer_mc) > 0)
								@php
								$sum_right_mc += $mcq->weight;
								@endphp
							@endif
						<td class="correct-answer">{{ strtoupper($alphabet->alphabet) }}. {{ $answer->answer }}</td>
						@else
							@if(count($student_answer_mc) > 0)
							<td class="choosen-answer">{{ strtoupper($alphabet->alphabet) }}. {{ $answer->answer }}</td>
							@else
							<td>{{ strtoupper($alphabet->alphabet) }}. {{ $answer->answer }}</td>
							@endif
						@endif
					</tr>
					@endforeach
				</table>
			</div>
		</div>
		@endforeach
		<!-- <div class="question">
			Berapa hasil 10 ditambah 10?
			<span class="weight">
				(Weight: 25%)
			</span>
			<div class="answer">
				<table>
					<tr>
						<td class="choosen-answer">A. 10</td>
						<td class="correct-answer">B. 20</td>
					</tr>
					<tr>
						<td>C. 30</td>
						<td>D. 40</td>
					</tr>
					<tr>
						<td>E. 50</td>
					</tr>
				</table>
			</div>
		</div> -->
	</div>
</div>
<div>
	Total score from multiple choice: {{ $sum_right_mc }}
</div>
</div>

<form action="{{ route('teacherAssessStudentExamProcess',['id'=>$exam->id, 'student_id'=>$student->id]) }}" method="post">
{{ csrf_field() }}
<div class="essay-wrapper">
<h3 style="text-align:center">Essay</h3>
@php
$no = 1;
@endphp
@foreach($eq as $eqs)
<div>
	<div class="essay">
		<div class="question">
			{{ $eqs->question }}
			<span class="weight">
				(Weight: {{ $eqs->weight }}%)
			</span>
			<span class="keywords">
				@php
				$essay_keywords = App\EssayKeyword::where('essay_id', $eqs->id)->get();
				$essay_no = 0;
				@endphp
				Keyword:
				@foreach($essay_keywords as $essay_keyword)
				@php
				$essay_no++;
				@endphp
				@if($essay_no == count($essay_keywords))
				{{ $essay_keyword->keyword }}
				@else
				{{ $essay_keyword->keyword }} |
				@endif
				@endforeach
			</span>
		</div>
		@php
		$answer_student = App\StudentEssayAnswer::where('essay_question_id', $eqs->id)->first();
		@endphp
		<div class="answer" id="essay-{{ $eqs->id }}">
			@if(count($answer_student) > 0)
			@php
			$keyword = [];
			$keyword_array = [];
			$keyword_array_highlight = [];
			foreach ($essay_keywords as $essay_keyword) {
				array_push($keyword,$essay_keyword->keyword);
				array_push($keyword_array,'<exam:replace>'.$essay_keyword->keyword.'</exam:replace>');
				array_push($keyword_array_highlight,'<span class="highlight-keyword">'.$essay_keyword->keyword.'</span>');
			}
			$keyword_replace_first = str_replace($keyword, $keyword_array, $answer_student->answer);
			@endphp
			{!! str_replace($keyword_array, $keyword_array_highlight, $keyword_replace_first) !!}
			@endif
		</div>
	</div>
	<input type="number" name="score-essay-{{ $eqs->id }}" placeholder="Score Essay {{ $no++ }}" min="0" max="{{ $eqs->weight }}">
</div>
@endforeach


<div style="margin-top:5%;text-align:center">
<input class="button" type="submit" value="Submit All Score">
</div>

</form>

@endsection
