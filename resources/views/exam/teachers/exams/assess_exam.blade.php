@extends('exam.layouts.app')

@section('content')

<div class="exam-data">
<div>{{ $exam->title }} kelas {{ $classroom->name }}</div>
<div>{{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->start)->format('j F Y H:i:s') }} - {{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->end)->format('j F Y H:i:s') }}</div>
</div>

<div class="table-wrapper">
	<h3>Student List</h3>
	<table border="1">
		<tr>
			<th>Student ID</th>
			<th>Student Name</th>
			<th>Score</th>
			<th>Action</th>
		</tr>
		@foreach($student_exams as $student_exam)
		@php
		$student = App\Student::find($student_exam->student_id);
		@endphp
		<tr>
			<td>{{ $student_exam->id }}</td>
			<td>{{ $student->name }}</td>
			<td>{{ $student_exam->score }}</td>
			<td>
				<a href="{{ route('teacherAssessStudentExam',['id'=>$exam->id, 'student_id'=>$student->id]) }}" class="button">Assess</a>
			</td>
		</tr>
		@endforeach
	</table>
</div>

@endsection
