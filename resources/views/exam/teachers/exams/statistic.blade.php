@extends('exam.layouts.app')

@section('content')

<div class="table-wrapper">
	<h3>Exam Statistic</h3>
	<table border="1">
		<tr>
			<th>Title</th>
			<th>Classroom</th>
			<th>Start Time</th>
			<th>End Time</th>
			<th>Min</th>
			<th>Max</th>
			<th>Mean</th>
		</tr>
    @foreach($exams as $exam)
    @php
    $classroom = App\Classroom::find($exam->classroom_id);
    $select_min = App\StudentExam::where('exam_id', $exam->id)->where('status','already completed')->min('score');
    $select_max = App\StudentExam::where('exam_id', $exam->id)->where('status','already completed')->max('score');
    $select_avg = App\StudentExam::where('exam_id', $exam->id)->where('status','already completed')->avg('score');
    @endphp
    <tr>
			<td>{{ $exam->title }}</td>
			<td>{{ $classroom->name }}</td>
      <td>{{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->start)->format('j F Y H:i:s') }}</td>
      <td>{{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->end)->format('j F Y H:i:s') }}</td>
      @if(count($select_min) != 0)
      <td>{{ $select_min }}</td>
      @else
      <td>-</td>
      @endif
      @if(count($select_max) != 0)
      <td>{{ $select_max }}</td>
      @else
      <td>-</td>
      @endif
      @if(count($select_avg) != 0)
      <td>{{ $select_avg }}</td>
      @else
      <td>-</td>
      @endif
		</tr>
    @endforeach
		<!-- <tr>
			<td>UTS Matematika</td>
			<td>10B</td>
			<td>11 Agustus 2017 13:00:00</td>
			<td>11 Agustus 2017 15:00:00</td>
			<td>-</td>
			<td>-</td>
			<td>-</td>
		</tr> -->
	</table>
</div>
<div class="table-wrapper">
	<h3>Student Statistic</h3>
	<table border="1">
		<tr>
			<th>Classroom</th>
			<th>Min</th>
			<th>Max</th>
			<th>Mean</th>
			<th>Action</th>
		</tr>
    @foreach($classrooms as $classroom)
    @php
		$student_exam_min = App\StudentExam::where('classroom_id', $classroom->id)->where('status','already completed')->min('score');
		$student_exam_max = App\StudentExam::where('classroom_id', $classroom->id)->where('status','already completed')->max('score');
		$student_exam_avg = App\StudentExam::where('classroom_id', $classroom->id)->where('status','already completed')->avg('score');
    @endphp
    <tr>
			<td>{{ $classroom->name }}</td>
			@if(count($student_exam_min) == 0)
			<td>-</td>
			@else
			<td>{{ $student_exam_min }}</td>
			@endif
			@if(count($student_exam_max) == 0)
			<td>-</td>
			@else
			<td>{{ $student_exam_max }}</td>
			@endif
			@if(count($student_exam_avg) == 0)
			<td>-</td>
			@else
			<td>{{ $student_exam_avg }}</td>
			@endif
			<td><a href="{{ route('teacherStatisticRankList',$classroom->id) }}" class="button">View detail</a></td>
		</tr>
    @endforeach
	</table>
</div>

@endsection
