@extends('exam.layouts.app')

@section('content')

<div class="table-wrapper">
	<h3>Assess Exam</h3>
	<table border="1">
		<tr>
			<th>Title</th>
			<th>Classroom</th>
			<th>Start Time</th>
			<th>End Time</th>
			<th>Action</th>
		</tr>
		@foreach($exams as $exam)
		@php
		$classroom = App\Classroom::find($exam->classroom_id);
		$student_exam = App\StudentExam::where('exam_id',$exam->id)->where('status','already completed')->count();
		@endphp
		<tr>
			<td>{{ $exam->title }}</td>
			<td>{{ $classroom->name }}</td>
			<td>{{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->start)->format('j F Y H:i:s') }}</td>
			<td>{{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->end)->format('j F Y H:i:s') }}</td>
			<td>
				@if($student_exam == 0)
				<a class="button disabled">Assess</a>
				@else
				<a href="{{ route('teacherAssessExam',$exam->id) }}" class="button">Assess</a>
				@endif
			</td>
		</tr>
		@endforeach
	</table>
</div>

@endsection
