@extends('exam.layouts.app')

@section('content')

<div class="table-wrapper">
	<h3>{{ $classroom->name }} Rank</h3>
	<table border="1">
		<tr>
			<th>Rank</th>
			<th>Student ID</th>
			<th>Student Name</th>
			<th>Average Score</th>
		</tr>
    @foreach($student_exams as $student_exam)
    @php
    $student = App\Student::find($student_exam->student_id);
    @endphp
    <tr>
			<td>{{ $no++ }}</td>
			<td>{{ $student_exam->student_id }}</td>
			<td>{{ $student->name }}</td>
			<td>{{ $student_exam->avg_score }}</td>
		</tr>
    @endforeach
	</table>
</div>

@endsection
