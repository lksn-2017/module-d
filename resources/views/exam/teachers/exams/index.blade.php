@extends('exam.layouts.app')

@section('content')

<div class="form-wrapper">
	<form action="{{ route('teacherPostExam') }}" method="POST">
		{{ csrf_field() }}
		<div>
			<input type="text" name="title" placeholder="Exam Title">
		</div>

		<div>
			<select name="classroom_id">
				<!-- get options from database -->
				<option selected value="">--Select Classroom--</option>
				@foreach($classrooms as $classroom)
				<option value="{{ $classroom->id }}">{{ $classroom->name }}</option>
				@endforeach
			</select>
		</div>
		<div>
			<div class="input-title">Start Exam Time</div>
			<input type="date" name="start_date">
			<input type="time" name="start_time">
		</div>
		<div>
			<div class="input-title">End Exam Time</div>
			<input type="date" name="end_date">
			<input type="time" name="end_time">
		</div>
		<div>
			<input type="submit" value="Create New Exam" class="button">
		</div>
	</form>
	@include('errors.message')
</div>
<div class="table-wrapper">
	<h3>Manage Exam Question</h3>
	<table border="1">
		<tr>
			<th>Title</th>
			<th>Classroom</th>
			<th>Start Time</th>
			<th>End Time</th>
			<th>Action</th>
		</tr>
		@foreach($exams as $exam)
		@php
		$classroom = App\Classroom::find($exam->classroom_id);
		@endphp
		<tr>
			<td>{{ $exam->title }}</td>
			<td>{{ $classroom->name }}</td>
			<td>{{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->start)->format('j F Y H:i:s') }}</td>
			<td>{{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->end)->format('j F Y H:i:s') }}</td>
			<td>
				<a href="{{ route('teacherManageExam',$exam->id) }}" class="button">Manage Question</a>
				<a href="{{ route('teacherDestroyExam',$exam->id) }}" class="button">Delete</a>
			</td>
		</tr>
		@endforeach
	</table>
</div>

@endsection
