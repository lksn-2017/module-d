
<!DOCTYPE html>
<html>
<head>
	<title>Login - Exam</title>
	@include('exam.layouts.asset')
</head>
<body>
<div class="form-wrapper">
<form action="{{ route('postLogin') }}" method="post">
	{{ csrf_field() }}
	<div>
		<input type="text" name="username" placeholder="Username">
	</div>
	<div>
		<input type="password" name="password" placeholder="Password">
	</div>
	<div>
		<input type="submit" value="Login" class="button">
	</div>
</form>
@include('errors.message')
</div>
</body>
</html>
