@extends('exam.layouts.app')

@section('content')

<div class="form-wrapper">
	<form action="{{ route('studentChangePassword') }}" method="POST">
		{{ csrf_field() }}
		<div>
			<input type="password" name="old_password" placeholder="Old Password">
		</div>
		<div>
			<input type="password" name="new_password" placeholder="New Password">
		</div>
		<div>
			<input type="password" name="conf_password" placeholder="New Password (Confirmation)">
		</div>
		<div>
			<input type="submit" value="Change Password" class="button">
		</div>
	</form>
	@include('errors.message')
</div>

@endsection
