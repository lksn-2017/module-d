@extends('exam.layouts.app')

@section('content')

<div class="table-wrapper">
	<h3>Exam List</h3>
	<table border="1">
		<tr>
			<th>Title</th>
			<th>Classroom</th>
			<th>Start Exam Time</th>
			<th>End Exam Time</th>
			<th>Action</th>
		</tr>
    @foreach($student_exams as $student_exam)
    @php
    $exam = App\Exam::find($student_exam->exam_id);
    $classroom = App\Classroom::find($exam->classroom_id);
    @endphp
    <tr>
			<td>{{ $exam->title }}</td>
			<td>{{ $classroom->name }}</td>
			<td>{{ Carbon::createFromFormat('Y-m-d H:i:s', $exam->start)->format('j F Y H:i:s') }}</td>
			<td>{{ Carbon::createFromFormat('Y-m-d H:i:s', $exam->end)->format('j F Y H:i:s') }}</td>
			<td>
        @if(date('Y-m-d H:i:s') > $exam->start)
				<a href="{{ route('studentTakeExam',$student_exam->id) }}" class="button">Take</a>
        @else
        <a class="button disabled">Take</a>
        @endif
			</td>
		</tr>
    @endforeach
	</table>
</div>

@endsection
