@extends('exam.layouts.app')

@section('content')

<div class="table-wrapper">
	<h3>Your Score</h3>
	<table border="1">
		<tr>
			<th>Title</th>
			<th>Start Exam Time</th>
			<th>End Exam Time</th>
			<th>Score</th>
		</tr>
		@foreach($student_exams as $student_exam)
		@php
		$exam = App\Exam::find($student_exam->exam_id);
		@endphp
		<tr>
			<td>{{ $exam->title }}</td>
			<td>{{ Carbon::createFromFormat('Y-m-d H:i:s', $exam->start)->format('j F Y H:i:s') }}</td>
			<td>{{ Carbon::createFromFormat('Y-m-d H:i:s', $exam->end)->format('j F Y H:i:s') }}</td>
			<td>
				{{ $student_exam->score }}
			</td>
		</tr>
		@endforeach
	</table>
	<div style="margin-top:1%">
	<div>Average Score: {{ $avg_score }}</div>
	<div>Classroom Rank: {{ $rank_in_class }} of {{ $student_in_class }}</div>
	<div>School Rank: {{ $rank_in_school }} of {{ $student_in_school }}</div>
	</div>
</div>

@endsection
