@extends('exam.layouts.app')

@section('content')

<div class="exam-data">
<div>{{ $exam->title }} kelas {{ $classroom->name }}</div>
<div>{{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->start)->format('j F Y H:i:s') }} - {{ Carbon::createFromFormat('Y-m-d H:i:s',$exam->end)->format('j F Y H:i:s') }}</div>
</div>

<form action="{{ route('studentSubmitAnswer',$student_exam->id) }}" method="post">
{{ csrf_field() }}
<div class="multiple-choice-wrapper">
<h3 style="text-align:center">Multiple Choice</h3>
<div>
	<div class="multiple-choice">
		@foreach($mc as $mcq)
		<div class="question">
			{{ $mcq->question }}
			<span class="weight">
				(Weight: {{ $mcq->weight }}%)
			</span>
			<div class="answer">
				<table>
					@php
					$answers = App\MultipleChoiceOption::where('multiple_choice_id',$mcq->id)->inRandomOrder()->get();
					@endphp
					@foreach($answers as $answer)
					<tr>
						<td><input type="radio" name="choice-{{ $mcq->id }}" value="{{ $answer->id }}">{{ $answer->answer }}</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
		@endforeach
	</div>
</div>
</div>
<div class="essay-wrapper">
<h3 style="text-align:center">Essay</h3>
<div>
	@foreach($eq as $eq_data)
	<div class="essay">
		<div class="question">
			{{ $eq_data->question }}
			<span class="weight">
				(Weight: {{ $eq_data->weight }}%)
			</span>
		</div>
		<div class="answer">
			<textarea name="essay-{{ $eq_data->id }}">
			</textarea>
		</div>
	</div>
	@endforeach
</div>

<div style="margin-top:5%;text-align:center">
<input type="submit" value="Submit Answers" class="button">
</div>

</form>

@endsection
